<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('cast')->get();
        return view('cast.index', [
            'title' => 'Cast',
            'url'   => 'cast',
            'cast'  => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cast.create', [
            'title' => 'Cast - Tambah Data',
            'url'   => 'cast',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama'  => ['required','unique:cast'],
            'umur'  => ['required','numeric'],
            'bio'   => ['required'],
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request->nama,
            "umur"  => $request->umur,
            "bio"   => $request->bio,
        ]);

        $request->session()->flash('info', "Data {$request->nama} berhasil ditambahkan!");

        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = DB::table('cast')->find($id);
        return view('cast.show', [
            'title' => 'Cast - Show Data',
            'url'   => 'cast',
            'cast'  => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = DB::table('cast')->find($id);
        return view('cast.edit', [
            'title' => 'Cast - Edit Data',
            'url'   => 'cast',
            'cast'  => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //
        $request->validate([
            'nama'  => ['required', Rule::unique('cast')->ignore($id, 'id')],
            'umur'  => ['required','numeric'],
            'bio'   => ['required'],
        ]);

        DB::table('cast')->where('id',$id)->update([
            "nama" => $request->nama,
            "umur"  => $request->umur,
            "bio"   => $request->bio,
        ]);

        $request->session()->flash('info', "Data {$request->nama} berhasil diubah!");

        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        //
        DB::table('cast')->where('id',$id)->delete();
        $request->session()->flash('info', "Data ID {$id} berhasil dihapus!");
        return redirect('/cast');
    }
}
