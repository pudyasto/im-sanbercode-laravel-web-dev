<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    //

    public function table(Request $request)
    {
        return view('table', [
            'title' => 'Table',
            'url'   => 'table',
        ]);
    }

    public function dataTable(Request $request)
    {
        return view('tabledata', [
            'title' => 'Data Table',
            'url'   => 'data-table',
        ]);
    }
}
