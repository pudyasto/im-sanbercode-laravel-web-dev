
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('adminlte3/dist/img/avatar.png') }}" class="img-circle elevation-2"
                alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">Pudyasto Adi Wibowo</a>
        </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
            data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
   with font-awesome or any other icon font library -->

            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('cast') }}" class="nav-link {{ isset($url) && $url =='cast' ? 'active' : '' }}">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Cast
                    </p>
                </a>
            </li>
            <li class="nav-item {{ isset($url) && in_array($url,['table', 'data-table']) ? 'menu-open' : '' }}">
                <a href="#" class="nav-link {{ isset($url) && in_array($url,['table', 'data-table']) ? 'active' : '' }}">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Table
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('table') }}" class="nav-link {{ isset($url) && $url =='table' ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Table</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('data-table') }}" class="nav-link {{ isset($url) && $url =='data-table' ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Data Table</p>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>