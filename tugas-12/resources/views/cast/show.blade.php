@extends('layout.master')

@section('content')
    <div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <div>
                    {{ $cast->nama }}
                </div>
            </div>

            <div class="form-group">
                <label for="umur">Umur</label>
                <div>
                    {{ $cast->umur }} Tahun
                </div>
            </div>

            <div class="form-group">
                <label for="bio">Biodata</label>
                <div>
                    {{ $cast->bio }}
                </div>
            </div>
            <a href="{{ route('cast') }}" class="btn btn-secondary">Kembali</a>
    </div>
@endsection
