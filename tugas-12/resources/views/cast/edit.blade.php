@extends('layout.master')

@section('content')
    <div>
        <form action="{{ route('cast.update', $cast->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama" value="{{ $cast->nama }}" required>
                @error('nama')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="umur">Umur (Tahun)</label>
                <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan umur" value="{{ $cast->umur }}" required>
                @error('umur')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="bio">Biodata</label>
                <textarea class="form-control" name="bio" id="bio" rows="4" placeholder="Masukkan biodata" required>{{ $cast->bio }}</textarea>
                @error('bio')
                    <div class="alert alert-danger mt-2">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-info">Ubah</button>
            <a href="{{ route('cast') }}" class="btn btn-secondary">Batal</a>
        </form>
    </div>
@endsection
