@extends('layout.master')

@section('content')
    @if (Session::has('info'))
        <div class="alert alert-info">
            {{ Session::get('info') }}
        </div>
    @endif

    <div class="mb-3">
        <a href="{{ route('cast.create') }}" class="btn btn-primary">Tambah</a>
        <a href="{{ route('cast') }}" class="btn btn-secondary">Refresh</a>
    </div>
    <table id="cast-data-datatable" class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center" style="width: 10px">#</th>
                <th>Nama</th>
                <th class="text-center" style="width: 90px">Umur</th>
                <th>Biodata</th>
                <th class="text-center" style="width: 40px">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
                <tr>
                    <td class="text-center">{{ $key + 1 }}</th>
                    <td>{{ $value->nama }}</td>
                    <td class="text-center">{{ $value->umur }} Tahun</td>
                    <td>{{ $value->bio }}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm rounded-0">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm rounded-0" value="Delete">
                            </form>
                        </div>
                    </td>
                </tr>
            @empty
            @endforelse
        </tbody>
    </table>
@endsection

@push('scripts')
    <script src="{{ asset('adminlte3/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte3/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#cast-data-datatable").DataTable();
        });
    </script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
