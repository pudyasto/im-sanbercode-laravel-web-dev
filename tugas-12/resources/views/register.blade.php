<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Register - Tugas 12</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name="author" content="Pudyasto Adi Wibowo">
    <meta name="description" content="Tugas 12 - Laravel SanberCode">
</head>
<body>
    <h1>
        Buat Account Baru!
    </h1>
    <h3>Sign Up Form</h3>
    <form action="{{ route('welcome') }}" method="post">
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="first_name" required placeholder="Please input first name"><br><br>

        <label>Last Name :</label><br>
        <input type="text" name="last_name" required placeholder="Please input last name"><br><br>

        <label>Gender :</label><br>
        <input type="radio" name="rad_gender" id="rad_gender_male" value="Male">
        <label for="rad_gender_male">Male</label><br>
        <input type="radio" name="rad_gender" id="rad_gender_female" value="Female">
        <label for="rad_gender_female">Female</label><br>
        <input type="radio" name="rad_gender" id="rad_gender_other" value="Other">
        <label for="rad_gender_other">Other</label><br><br>

        <label>Nationality :</label><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>

        <label>Language Spoken :</label><br>
        <input type="checkbox" name="chk_lang[]" id="chk_lang_id" value="Bahasa Indonesia">
        <label for="chk_lang_id">Bahasa Indonesia</label><br>
        <input type="checkbox" name="chk_lang[]" id="chk_lang_en" value="English">
        <label for="chk_lang_en">English</label><br>
        <input type="checkbox" name="chk_lang[]" id="chk_lang_other" value="Other">
        <label for="chk_lang_other">Other</label><br><br>

        <label>Bio :</label><br>
        <textarea name="bio" rows="5" required placeholder="Please insert bio"></textarea><br>

        <button type="submit" name="submit" value="submit">Sign Up</button>
    </form>
</body>
</html>