<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Home - Tugas 12</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name="author" content="Pudyasto Adi Wibowo">
    <meta name="description" content="Tugas 12 - Laravel SanberCode">
</head>
<body>
    <h1>
        SanberBook
    </h1>
    <h2>
        Social Media Developer Santai Berkualitas
    </h2>
    <p>
        Belajar dan Berbagi agar hidup ini semakin berkualitas
    </p>
    <h3>
        Benefit Join di SanberBook
    </h3>
    <ul>
        <li>
            Mendapatkan motivasi dari sesama developer
        </li>
        <li>
            Sharing knowledge dari para mastah Sanber
        </li>
        <li>
            Dibuat lieh calon web developer terbaik
        </li>
    </ul>
    <h3>
        Cara Bergabung ke SanberBook
    </h3>
    <ol>
        <li>
            Mengunjungi Website ini
        </li>
        <li>
            Mendaftar di <a href="{{ route('register') }}">Form Sign Up</a>
        </li>
        <li>
            Selesai
        </li>
    </ol>
</body>
</html>