<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Welcome - Tugas 12</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name="author" content="Pudyasto Adi Wibowo">
    <meta name="description" content="Tugas 12 - Laravel SanberCode">
</head>
<body>
    <h1>
        Selamat Datang, {{isset($data->first_name) ? $data->first_name : ''}} {{isset($data->last_name) ? $data->last_name : ''}}
    </h1>
    <h2>
        Terima kasih telah bergabung di SanberBook. Social Media kita bersama!
    </h2>
</body>
</html>