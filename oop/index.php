<?php

require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name; // "shaun"
echo "<br>Legs : " . $sheep->legs; // 4
echo "<br>Cold Blooded : " . $sheep->cold_blooded; // "no"

echo "<br><br>";
$frog = new Frog("buduk");
echo "Name : " . $frog->name; // "buduk"
echo "<br>Legs : " . $frog->legs; // 2
echo "<br>Cold Blooded : " . $frog->cold_blooded; // "no"
echo "<br>Jump : " . $frog->jump(); // "no"

echo "<br><br>";
$ape = new Ape("kera sakti");
echo "Name : " . $ape->name; // "kera sakti"
echo "<br>Legs : " . $ape->legs; // 2
echo "<br>Cold Blooded : " . $ape->cold_blooded; // "no"
echo "<br>Yell : " . $ape->yell(); // "no"